import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class GeneratorGrafow {
	

	public static void main(String[] args) throws FileNotFoundException {
		int[] sizes = {10, 50, 100, 200, 300};
		double[] density = {0.25, 0.5, 0.75, 1};
		
		int quantity = 1;
		
		int currentSize;
		double currentDensity;
		
		for(int s = 0; s < sizes.length; s++){
			currentSize = sizes[s];
			for(int d = 0; d < density.length; d++){
				currentDensity = density[d];
				for(int i = 0; i < quantity; i++ ){
					PrintWriter plik = new PrintWriter("out_"+currentSize+"_"+((int)(currentDensity*100))+"_"+i);
					ArrayList<Edge> edges = generateGraph(currentSize, currentDensity);
					writeFile(edges, plik);
					plik.close();
				}
			}
		}
		
	}
	
	public static void writeFile(ArrayList<Edge> edges, PrintWriter plik){
		for(int i = 0; i<edges.size(); ++i){
			Edge tmp = edges.get(i);
			plik.println(tmp.getFirstVertex()+" "+tmp.getLastVertex()+" "+tmp.getWeight());
		}
	}
	
	public static ArrayList<Edge> generateGraph(int currentSize, double currentDensity){
		
		Random r = new Random();
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for(int i = 0; i < currentSize; i++){
			list.add(i);
		}
		
		Collections.shuffle(list);
		
		ArrayList<Edge> edges = new ArrayList<Edge>();
		
		for(int i = 0; i < list.size()-1; ++i){
			edges.add(new Edge(list.get(i), list.get(i+1), Math.abs(r.nextInt(100)+1)));
		}
		
		
		int edgesToGo =(int) ((currentDensity/2)*currentSize*(currentSize-1) - edges.size());
		
		for(int i = 0; i < edgesToGo; ++i){
			Edge ed;
			do{
				ed = new Edge(r.nextInt(list.size()), r.nextInt(list.size()), Math.abs(r.nextInt(100)));
			}while(isDuplicated(edges, ed));
			edges.add(ed);
		}
		
		return edges;
		
	}
	
	public static boolean isDuplicated(ArrayList<Edge> edges, Edge edge){
		for(int i = 0; i<edges.size(); i++){
			Edge tmp = edges.get(i);
			if(tmp.getFirstVertex() == edge.getFirstVertex() && tmp.getLastVertex() == edge.getLastVertex()){
				return true;
			}
			if(tmp.getLastVertex() == edge.getFirstVertex() && tmp.getFirstVertex() == edge.getLastVertex()){
				return true;
			}
		}
		return false;
	}

}
