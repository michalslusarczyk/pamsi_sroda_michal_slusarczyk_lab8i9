class Leaf{
private:
    Leaf* next;
    int vertexID;
    int weight;
public:

    Leaf(){
        next = NULL;
        vertexID = -1;
        weight = 0;
    }

    void setNext(Leaf* n){
        next = n;
    }

    Leaf* getNext(){
        return next;
    }

    int getVertex(){
        return vertexID;
    }

    int getWeight(){
        return weight;
    }

    void setVertex(int id){
        vertexID = id;
    }

    void setWeight(int w){
        weight = w;
    }
};

class Tree{
private:
    //Leaves array
    Leaf** leaves;
    //Size of array of leaves
    int size;
    //Tree weight
    int weight;
public:
    Tree(int n){
        leaves = new Leaf*[n];
        for(int i = 0; i < n; i++){
            leaves[i] = NULL;
        }
        size = n;
        weight = 0;
    }
    void addEdge(Edge* e){
        weight += e->getWeight();
        Leaf* p = new Leaf();

        p->setVertex(e->getLastVertex());
        p->setWeight(e->getWeight());
        p->setNext(leaves[e->getFirstVertex()]);
        leaves[e->getFirstVertex()] = p;


        p = new Leaf();
        p->setVertex(e->getFirstVertex());
        p->setWeight(e->getWeight());
        p->setNext(leaves[e->getLastVertex()]);
        leaves[e->getLastVertex()] = p;

    }

    void print(){
        cout<<"Weight of minimal spanning tree: "<< weight<<"\n";
        Leaf* tmp;
        for(int i = 0; i < size; ++i){
            for(tmp = leaves[i]; tmp != NULL; tmp = tmp->getNext()){
                if(tmp->getVertex() > i){
                    cout << "[" << i << "] --- ("<< tmp->getWeight() <<") --- ["<< tmp->getVertex() <<"]\n";
                }
            }
        }
    }

    Leaf* getLeaves(int n){
        return leaves[n];
    }
};
