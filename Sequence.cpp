#include <string>
#include <iostream>
using namespace std;

template <typename T>
class SequenceElement;

template <typename T>
class Sequence{
public:
    const std::string SequenceEmptyException = "Sequence is Empty";
    const std::string OutOfBoundsException = "Out of Bounds";

    Sequence();
    int getSize();
    bool isEmpty();

    SequenceElement<T>* atIndex(int i);
    int indexOf(SequenceElement<T>* p);

    void addFront(SequenceElement<T>* v);
    void addRear(SequenceElement<T>* v);
    SequenceElement<T>* addAfter(SequenceElement<T>* p, SequenceElement<T>* v);
    SequenceElement<T>* addBefore(SequenceElement<T>* p, SequenceElement<T>* v);

    T getFront();
    SequenceElement<T>* getFrontElement();
    T getRear();

    T removeFront();
    T remove(SequenceElement<T>* p);
    T removeRear();

protected:
    int sizeOfSequence;
    SequenceElement<T>* first;
    SequenceElement<T>* rear;
};

template <typename T>
Sequence<T>::Sequence(){
    sizeOfSequence = 0;
    first = NULL;
    rear = NULL;
}

template <typename T>
bool Sequence<T>::isEmpty(){
    return getSize()==0;
}

template <typename T>
SequenceElement<T>* Sequence<T>::atIndex(int i){
    if(i>sizeOfSequence-1){
        throw OutOfBoundsException;
    }else{
        SequenceElement<T>* temp = first;
        for(int k = 0; k<i; ++k){
                temp = temp->getNext();
        }

        return temp;
    }
}
template <typename T>
int Sequence<T>::indexOf(SequenceElement<T>* p){
    SequenceElement<T>* temp = first;
    for(int i = 0; i<sizeOfSequence; ++i){
        if(temp == p){
            return i;
        }
        temp = temp->getNext();
    }
    if(temp == NULL){
        throw OutOfBoundsException;
    }else{
        return sizeOfSequence-1;
    }
}

template <typename T>
int Sequence<T>::getSize(){
    return sizeOfSequence;
}

template <typename T>
T Sequence<T>::getFront(){
    if(sizeOfSequence == 0)
        throw SequenceEmptyException;
    else
        return first->getElement();
}

template <typename T>
SequenceElement<T>* Sequence<T>::getFrontElement(){
    if(sizeOfSequence == 0)
        throw SequenceEmptyException;
    else
        return first;
}

template <typename T>
T Sequence<T>::getRear(){
    if(sizeOfSequence == 0)
        throw SequenceEmptyException;
    else
        return rear->getElement();
}

template <typename T>
void Sequence<T>::addFront(SequenceElement<T>* v){
    if(sizeOfSequence == 0){
        first = v;
        rear = v;
    }else{
        first->setPrev(v);
        v->setNext(first);
        first = v;
    }
    ++sizeOfSequence;
}

template <typename T>
void Sequence<T>::addRear(SequenceElement<T>* v){
    if(sizeOfSequence == 0){
        first = v;
        rear = v;
    }else{
        rear->setNext(v);
        v->setPrev(rear);
        rear = v;
    }
    ++sizeOfSequence;
}

template <typename T>
T Sequence<T>::removeFront(){
    if(sizeOfSequence == 0){
        return NULL;
        throw SequenceEmptyException;
    }else{
        T value = first->getElement();
        SequenceElement<T>* temp = first;
        if(sizeOfSequence == 1){
            first = NULL;
            rear = NULL;
        }else{
            cout<<first->getNext()->getElement()<<"\n";
            first -> getNext() -> setPrev(NULL);
            first = first -> getNext();
        }
        --sizeOfSequence;
        //delete temp;
        return value;
    }
}

template <typename T>
T Sequence<T>::removeRear(){
    if(sizeOfSequence == 0){
        throw SequenceEmptyException;
    }else{
        T value = rear->getElement();
        SequenceElement<T>* temp = rear;
        if(sizeOfSequence == 1){
            first = NULL;
            rear = NULL;
        }else{
            rear -> getPrev() -> setNext(NULL);
            rear = rear -> getPrev();
        }
        --sizeOfSequence;
        delete temp;
        return value;
    }
}

template <typename T>
SequenceElement<T>* Sequence<T>::addAfter(SequenceElement<T>* p, SequenceElement<T>* v){
    v->setPrev(p);
    v->setNext(p->getNext());
    p->getNext()->setPrev(v);
    p->setNext(v);
    sizeOfSequence++;
    return v;
}

template <typename T>
SequenceElement<T>* Sequence<T>::addBefore(SequenceElement<T>* p, SequenceElement<T>* v){
    v->setNext(p);
    v->setPrev(p->getPrev());
    if(p->getPrev()!= NULL)
        p->getPrev()->setNext(v);
    p->setPrev(v);
    sizeOfSequence++;
    return v;
}

template <typename T>
T Sequence<T>::remove(SequenceElement<T>* p){
    T temp = p->getElement();
    (p->getPrev())->setNext(p->getNext());
    (p->getNext())->setPrev(p->getPrev());
    p->setPrev(NULL);
    p->setNext(NULL);
    sizeOfSequence--;
    return temp;
}


//===================================================

template <typename T>
class SequenceElement{
public:
    T getElement();
    void setElement(T element);

    int getKey();
    void setKey(int k);

    SequenceElement<T>* getNext();
    void setNext(SequenceElement<T>* nxt);
    SequenceElement<T>* getPrev();
    void setPrev(SequenceElement<T>* prv);

    SequenceElement(){
    }
private:
    int key;
    T value;
    SequenceElement<T>* prev;
    SequenceElement<T>* next;
};

template <typename T>
T SequenceElement<T>::getElement(){
    return value;
}

template <typename T>
void SequenceElement<T>::setElement(T element){
    value = element;
}

template <typename T>
int SequenceElement<T>::getKey(){
    return key;
}

template <typename T>
void SequenceElement<T>::setKey(int k){
    key = k;
}

template <typename T>
SequenceElement<T>* SequenceElement<T>::getNext(){
    return next;
}

template <typename T>
void SequenceElement<T>::setNext(SequenceElement<T>* nxt){
    next = nxt;
}

template <typename T>
SequenceElement<T>* SequenceElement<T>::getPrev(){
    return prev;
}

template <typename T>
void SequenceElement<T>::setPrev(SequenceElement<T>* prv){
    prev = prv;
}

