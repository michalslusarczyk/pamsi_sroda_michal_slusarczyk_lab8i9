#include <iostream>
using namespace std;
template <typename T>
class ListElement{
public:
    T getElement();
    void setElement(T element);
    ListElement<T>* getNext();
    void setNext(ListElement<T>* nxt);
private:
    T value;
    ListElement<T>* next;
};

template <typename T>
T ListElement<T>::getElement(){
    return value;
}

template <typename T>
void ListElement<T>::setElement(T element){
    value = element;
}

template <typename T>
ListElement<T>* ListElement<T>::getNext(){
    return next;
}

template <typename T>
void ListElement<T>::setNext(ListElement<T>* nxt){
    next = nxt;
}

// LISTA
//===========================================

template <typename T>
class List{
protected:
    ListElement<T>* first;
    int size;
public:
    List();
    int getSize();
    bool isEmpty();
    T getFront();
    ListElement<T>* getFirst();
    void addFront(T t);
    ListElement<T>* removeFront();
    void display();
    void removeAll();
};

template <typename T>
List<T>::List(){
    first = NULL;
    size = 0;
}

template <typename T>
int List<T>::getSize(){
    return size;
}

template <typename T>
bool List<T>::isEmpty(){
    return size == 0;
}

template <typename T>
void List<T>::addFront(T t){
    ListElement<T>* v = new ListElement<T>;
    v->setElement(t);
    v->setNext(first);
    first = v;
    size++;
}

template <typename T>
T List<T>::getFront(){
    return first->getElement();
}

template <typename T>
ListElement<T>* List<T>::removeFront(){
    ListElement<T>* temp = first;
    first = temp->getNext();
    size--;
    return temp;
}

template <typename T>
ListElement<T>* List<T>::getFirst(){
    return first;
}

template <typename T>
void List<T>::display(){
    ListElement<T>* temp = first;
    for(int i=0; i<size; ++i){
        std::cout<<"Element nr "<<i<<"\nO wartosci :";
        std::cout<<temp->getElement()<<"\n";
        temp = temp->getNext();
    }
}

template <typename T>
void List<T>::removeAll(){
    while(!isEmpty()){
        removeFront();
    }
}

