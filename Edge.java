
public class Edge {

	private int firstVertex;
	public int getFirstVertex() {
		return firstVertex;
	}


	public void setFirstVertex(int firstVertex) {
		this.firstVertex = firstVertex;
	}


	public int getLastVertex() {
		return lastVertex;
	}


	public void setLastVertex(int lastVertex) {
		this.lastVertex = lastVertex;
	}


	public int getWeight() {
		return weight;
	}


	public void setWeight(int weight) {
		this.weight = weight;
	}


	private int lastVertex;
	private int weight;
	
	public Edge(int firstVertex, int lastVertex, int weight){
		this.firstVertex=firstVertex;
		this.lastVertex=lastVertex;
		this.weight=weight;
		
	}
	
	
	public String toString(){
		return firstVertex+"---("+weight+")---"+lastVertex;
	}
	
}
