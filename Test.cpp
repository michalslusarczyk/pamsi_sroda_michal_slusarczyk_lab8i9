#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <sstream>
#include <windows.h>
#include "Graph.cpp"
#include "Queue.cpp"
#include "Cluster.cpp"
#include "Tree.cpp"
#include "Algorithms.cpp"

using namespace std;

LARGE_INTEGER startTimer()
{
LARGE_INTEGER start;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&start);
 SetThreadAffinityMask(GetCurrentThread(), oldmask);
 return start;
}
LARGE_INTEGER endTimer()
{
LARGE_INTEGER stop;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&stop);
SetThreadAffinityMask(GetCurrentThread(), oldmask);
return stop;
}

template <typename T>
string to_string ( T Number )
{
	stringstream ss;
	ss << Number;
	return ss.str();
}

int main(){
    int sizes[] = {10, 50, 100, 200, 300};
    double density[] = {0.25, 0.5, 0.75, 1};
    int quantity = 100;

    int totalSize = 20*quantity;
    string* lines = new string[totalSize];
    //counter
    int o = 0;

    for(int s = 0; s < 5; s++){
        for(int d = 0; d < 4; d++){
            for(int q = 0; q < quantity; q++){
                int currentSize = sizes[s];
                double currentDensity = density[d];
                Graph* graph = new Graph(currentSize);

                int numberOfEdges = (int) ((currentDensity/2)*currentSize*(currentSize-1));
                std::ifstream plik;
                string path = "out_"+to_string(currentSize)+"_"+to_string(((int)(currentDensity*100)))+"_"+to_string(q);
                plik.open(path);
                for(int i = 0; i < numberOfEdges; i++){

                    int first, last, weight;
                    plik>>first;
                    plik>>last;
                    plik>>weight;
                    graph->insertEdge(new Edge(weight, first, last));
                }
                plik.close();
                LARGE_INTEGER performanceCountStart,performanceCountEnd;
                performanceCountStart = startTimer();
                kruskal(graph);
                performanceCountEnd = endTimer();
                double tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
                string result = "kruskal,";
                result+=to_string(currentSize);
                result+=",";
                result+=to_string(((int)(currentDensity*100)));
                result+=",";
                result+=to_string(tm);
                lines[o] = result;
                o++;
            }
        }
    }

    std::ofstream output;
    output.open("output_kruskal.csv");

    for(int a = 0; a < totalSize; ++a){
        output<<lines[a]<<"\n";
    }

    output.close();

    return 0;
}
