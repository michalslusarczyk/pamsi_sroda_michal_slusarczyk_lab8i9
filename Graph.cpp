#include "Sequence.cpp"
#include "List.cpp"
class Edge{
private:
    int weight;
    int firstVertex;
    int lastVertex;
public:
    Edge(int weight, int vertex1, int vertex2){
        this->weight = weight;
        firstVertex = vertex1;
        lastVertex = vertex2;
    }

    Edge(){
        weight = NULL;
        firstVertex = NULL;
        lastVertex = NULL;
    }

    int getWeight(){
        return weight;
    }

    void setWeight(int w){
        weight = w;
    }

    int getFirstVertex(){
        return firstVertex;
    }

    int getLastVertex(){
        return lastVertex;
    }

    void setFirstVertex(int f){
        firstVertex = f;
    }

    void setLastVertex(int l){
        lastVertex = l;
    }
};

class GraphElement{
private:
    Edge* edge;
    int vertexID;
public:
    GraphElement(int vertex, Edge* ed){
        vertexID = vertex;
        edge = ed;
    }
    Edge* getEdge(){
        return edge;
    }
    int getVertex(){
        return vertexID;
    }
    void setEdge(Edge* e){
        edge = e;
    }
    void setVertex(int id){
        vertexID = id;
    }
};

class Graph{
private:
    Sequence<Edge*>* edges;
    List<GraphElement*>** neighList;
    Edge*** neighMatrix;
    int graphSize;
    int edgesCount;
public:
    Graph(int n){
        graphSize = n;
        edgesCount = 0;
        edges = new Sequence<Edge*>();

        neighList = new List<GraphElement*>*[n];
        for(int i = 0; i < n; ++i){
            neighList[i] = new List<GraphElement*>();
        }

        neighMatrix = new Edge**[n];
        for(int i = 0; i < n; i++){
            neighMatrix[i] = new Edge*[n];
        }

         for(int i = 0; i < graphSize; ++i){
            for(int k = 0; k < graphSize; ++k){
                neighMatrix[i][k] = NULL;
            }
        }
    }

    int getSize(){
        return graphSize;
    }

    Edge* getEdge(int i){
        return edges->atIndex(i)->getElement();
    }

    int getEdgesCount(){
        return edgesCount;
    }

    void insertEdge(Edge* ed){
        SequenceElement<Edge*>* element = new SequenceElement<Edge*>();
        element->setElement(ed);
        element->setNext(NULL);
        element->setPrev(NULL);
        edges->addRear(element);

        int k1 = ed->getFirstVertex();
        int k2 = ed->getLastVertex();

        neighMatrix[k1][k2] = ed;
        neighMatrix[k2][k1] = ed;

        GraphElement* el = new GraphElement(k2, ed);
        neighList[k1]->addFront(el);

        GraphElement* el2 = new GraphElement(k1, ed);
        neighList[k2]->addFront(el2);

        edgesCount++;
    }

    Edge* getMatrixElement(int x, int y){
        return neighMatrix[x][y];
    }

    List<GraphElement*>* getNeighList(int i){
        return neighList[i];
    }

    void displayMatrix(){
        for(int i=0; i<graphSize; ++i){
            for(int k=0; k<graphSize; ++k){
                if(neighMatrix[i][k] == NULL){
                    std::cout<<"N\t";
                }else{
                    std:;cout<<neighMatrix[i][k]->getWeight()<<"\t";
                }
            }
            std::cout<<"\n";
        }
    }

};
