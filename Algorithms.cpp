
//Kruskal's algorithm
void kruskal(Graph* graph){
    //Size of graph
    int n = graph->getSize();
    //Edges count
    int m = graph->getEdgesCount();

    //Clusters
    Sets* clusters = new Sets(n);
    // heap-based priority queue
    PriorityQueue* Q = new PriorityQueue(m);
    // minimum spanning tree
    Tree* T = new Tree(n);

    for(int i = 0; i < n; i++){
        clusters->createCluster(i);
    }

    for(int i = 0; i < m; i++){
        Q->insert(graph->getEdge(i));
    }

    Edge* tmp;
    for(int i = 1; i < n; i++){
        do{
            tmp = Q->removeFront();
        }while(clusters->getSet(tmp->getFirstVertex()) == clusters->getSet(tmp->getLastVertex()));

        T->addEdge(tmp);
        clusters->setsUnion(tmp);
    }

    T->print();

}

// Prim's algorithm (graph with neighbourhood list)
void primList(Graph* graph){

    // Size of graph
    int n = graph->getSize();
    // Edges count
    int m = graph->getEdgesCount();

    // heap-based priority queue
    PriorityQueue* Q = new PriorityQueue(m);
    // minimum spanning tree
    Tree* T = new Tree(n);

    // list of visited vertices
    bool* visitList = new bool[n];

    // index of current vertex
    int currentVertex = 0;

    for(int i = 0; i < n; i++){
        visitList[i] = false;
    }

    visitList[currentVertex] = true;

    for(int i = 1; i < n; i++){
            ListElement<GraphElement*>* temp;
            Edge* temp2 = new Edge();
            for(temp = graph->getNeighList(currentVertex)->getFirst(); temp != NULL ; temp = temp->getNext()){
                if(!visitList[temp->getElement()->getVertex()]){
                    temp2->setFirstVertex(currentVertex);
                    temp2->setLastVertex(temp->getElement()->getVertex());
                    temp2->setWeight(temp->getElement()->getEdge()->getWeight());
                    Q->insert(temp2);
                }
            }

            do{
                temp2 = Q->removeFront();
            }while(visitList[temp2->getLastVertex()]);

            T->addEdge(temp2);
            visitList[temp2->getLastVertex()] = true;
            currentVertex = temp2->getLastVertex();
    }

    T->print();


}

// Prim's algorithm (graph with neighbourhood matrix)
void primMatrix(Graph* graph){

    // Size of graph
    int n = graph->getSize();
    // Edges count
    int m = graph->getEdgesCount();

    // heap-based priority queue
    PriorityQueue* Q = new PriorityQueue(m);
    // minimum spanning tree
    Tree* T = new Tree(n);

    // list of visited vertices
    bool* visitList = new bool[n];

    // index of current vertex
    int currentVertex = 0;

    for(int i = 0; i < n; i++){
        visitList[i] = false;
    }

    visitList[currentVertex] = true;

    for(int i = 1; i < n; i++){
            for(int j = 0; j < graph->getSize(); j++){
                Edge* tmp;
                Edge* tmp2 = new Edge();
                tmp = graph->getMatrixElement(currentVertex, j);
                if(tmp == NULL){
                    continue;
                }else{
                    if(!visitList[j]){
                        tmp2->setFirstVertex(currentVertex);
                        tmp2->setLastVertex(j);
                        tmp2->setWeight(tmp->getWeight());
                        Q->insert(tmp2);
                    }
                }
            }
            Edge* tmp;
            do{
                tmp = Q->removeFront();
            }while(visitList[tmp->getLastVertex()]);

            T->addEdge(tmp);
            visitList[tmp->getLastVertex()] = true;
            currentVertex = tmp->getLastVertex();
    }

    T->print();


}
