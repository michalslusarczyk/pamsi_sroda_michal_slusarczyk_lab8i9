#include <iostream>
#include <cstdlib>
#include "Graph.cpp"
#include "Queue.cpp"
#include "Cluster.cpp"
#include "Tree.cpp"
#include "Algorithms.cpp"

using namespace std;


int main(){


    Graph* graph = new Graph(6);

    graph->insertEdge(new Edge(2, 0, 5));
    graph->insertEdge(new Edge(2, 0, 3));
    graph->insertEdge(new Edge(2, 3, 5));
    graph->insertEdge(new Edge(3, 1, 5));
    graph->insertEdge(new Edge(1, 1, 3));
    graph->insertEdge(new Edge(2, 1, 2));
    graph->insertEdge(new Edge(1, 2, 4));
    graph->insertEdge(new Edge(3, 3, 2));

    graph->displayMatrix();

    primList(graph);

    primMatrix(graph);

    kruskal(graph);

    return 0;
}
