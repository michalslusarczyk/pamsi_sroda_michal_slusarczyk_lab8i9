class PriorityQueue{
private:
    //Array of pointers
    Edge** heap;
    //Last element index
    int currentPosition;
public:
    PriorityQueue(int n){
        heap = new Edge*[n];
        currentPosition = 0;
    }

    Edge* getFront(){
        return heap[0];
    }

    void insert(Edge* e){
        //Position for e element
        int i = currentPosition++;
        //Parent position
        int j = (i - 1) >> 1;
        while(i && (heap[j]->getWeight() > e->getWeight())){
            heap[i] = heap[j];
            i = j;
            j = (i - 1) >> 1;
        }
        heap[i] = e;
    }

    Edge* removeFront(){
        int i;
        int j;
        Edge* e;
        Edge* tmp = heap[0];
        if(currentPosition != 0){
            e = heap[--currentPosition];
            i = 0;
            j = 1;
            while(j < currentPosition){
                if((j + 1 < currentPosition) && (heap[j + 1]->getWeight() < heap[j]->getWeight())) j++;
                if(e->getWeight() <= heap[j]->getWeight()) break;
                heap[i] = heap[j];
                i = j;
                j = (j << 1) + 1;
            }
            heap[i] = e;
        }

        return tmp;
    }

    int getSize(){
        return currentPosition;
    }
};
