class Cluster{
private:
    int vertexID;
    int cost;
public:
    void setVertexID(int id){
        vertexID = id;
    }

    void setCost(int c){
        cost = c;
    }

    int getVertex(){
        return vertexID;
    }

    int getCost(){
        return cost;
    }
};

class Sets{
  private:
    Cluster* clusters;
  public:
    Sets(int n){
        clusters = new Cluster[n];
    }

    void createCluster(int v){
        clusters[v].setVertexID(v);
        clusters[v].setCost(0);
    }

    int getSet(int v){
        if(clusters[v].getVertex() != v){
            clusters[v].setVertexID(getSet(clusters[v].getVertex()));
        }
        return clusters[v].getVertex();
    }

    void setsUnion(Edge* e){
        int tmp1 = getSet(e->getFirstVertex());
        int tmp2 = getSet(e->getLastVertex());

        if(tmp1 != tmp2){
            if(clusters[tmp1].getCost() > clusters[tmp2].getCost()){
                clusters[tmp2].setVertexID(tmp1);
            }else{
                clusters[tmp1].setVertexID(tmp2);
                if(clusters[tmp1].getCost() == clusters[tmp2].getCost()){
                    clusters[tmp2].setCost(clusters[tmp2].getCost()+1);
                }
            }
        }
    }
};
